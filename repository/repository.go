package repository

import "database/sql"

type Repository struct {
	*sql.DB
}

// NewRepository create repository to connect into LTA database
func NewRepository(db *sql.DB) *Repository {
	return &Repository{db}
}

// Insert record into LTA database
func (r Repository) Insert(patientID string, studyinstanceuid string, imageinstance string, pathimages string) error {
	sqlStatement := ` INSERT INTO public.mirth
	(idpatient, studyinstanceuid, imageinstance, pathimages, mirthsent, validation)
	VALUES($1, $2, $3, $4, false, false) RETURNING id`
	var id int64
	err := r.QueryRow(sqlStatement, patientID, studyinstanceuid, imageinstance, pathimages).Scan(&id)
	if err != nil {
		return err
	}

	return nil
}
