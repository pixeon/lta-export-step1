package search

import (
	"bytes"
	"database/sql"
	"errors"
	"fmt"
	"strings"

	"github.com/rs/zerolog/log"
)

const (
	sqlStudy = `select p.patientid,st.studyinsta from dicompatients p 
join dicomstudies st on p.patientid = st.patientid `
	sqlDicom = `select
	device.no_localstore,
	i.objectfile as path,
	i.sopinstanc
from
	dicomstudies study
inner join dicomseries s on
	study.studyinsta = s.studyinsta
inner join dicomimages i on
	i.seriesinst = s.seriesinst
inner join devicestore device on device.no_devicestore = i.devicename
where s.studyinsta= $1`
)

// Filter used to search images and patient to send to LTA
type Filter struct {
	PatientID string
	DateInit  string
	DateEnd   string
	StudyUID  string
	Count     int
}

// Result the seach of images to export
type Result struct {
	PatientID string
	StudyUID  string
	Dicoms    [][2]string
}

// Search repository to connect into PACS database
type Search struct {
	*sql.DB
}

// NewSearch create repository to connect into PACS database
func NewSearch(db *sql.DB) *Search {
	return &Search{db}
}

// Search images
func (s Search) Search(filter *Filter) (chan Result, error) {
	sql, data, err := generateSQL(filter)
	if err != nil {
		return nil, err
	}
	ch := make(chan Result, 10)

	go func() {
		st, err := s.Prepare(sql)
		if err != nil {
			log.Error().Err(err).Msgf("error query from filter %v , sql: %s", filter, sql)
		}
		rows, err := st.Query(data...)
		if err != nil {
			log.Error().Err(err).Msgf("error query from filter %v , sql: %s", filter, sql)
		}

		for rows.Next() {
			result := Result{Dicoms: [][2]string{}}
			err = rows.Scan(&result.PatientID, &result.StudyUID)
			if err != nil {
				log.Error().Err(err).Msg("error rows.Scan study")
				continue
			}

			imgs, err := s.Query(sqlDicom, result.StudyUID)
			if err != nil {
				log.Error().Err(err).Msgf("error query imags StudyUID %s", result.StudyUID)
				continue
			}

			for imgs.Next() {
				var path string
				var storage string
				var insta string
				err = imgs.Scan(&storage, &path, &insta)
				if err != nil {
					log.Error().Err(err).Msgf("error rows.Scan path StudyUID %s", result.StudyUID)
					continue
				}
				result.Dicoms = append(result.Dicoms, [2]string{insta, storage + path})
			}
			log.Info().Msgf("Send to check channel study %s count img %d", result.StudyUID, len(result.Dicoms))
			ch <- result
		}
		close(ch)

	}()

	return ch, nil
}

func generateSQL(filter *Filter) (string, []interface{}, error) {
	data := []interface{}{}
	var buff bytes.Buffer

	if filter.Count < 1 && len(filter.PatientID) < 1 {
		return "", data, errors.New("filter.Count and filter.PatientID could not be null")
	}

	s := strings.Replace(sqlStudy, "\n", " ", -1)

	buff.WriteString(s)
	buff.WriteString("where (st.nu_ltaexported=0) ")
	i := 1

	if len(filter.PatientID) > 0 {
		buff.WriteString(fmt.Sprintf(" and p.patientid= $%v ", i))
		data = append(data, filter.PatientID)
		i++
	}

	if len(filter.StudyUID) > 0 {
		buff.WriteString(fmt.Sprintf(" and p.studyinsta= $%v ", i))
		data = append(data, filter.StudyUID)
		i++
	}

	if len(filter.DateInit) > 0 {
		buff.WriteString(fmt.Sprintf(" and st.studydate >= $%v ", i))
		data = append(data, filter.DateInit)
		i++
	}

	if len(filter.DateEnd) > 0 {
		buff.WriteString(fmt.Sprintf(" and st.studydate <= $%v ", i))
		data = append(data, filter.DateEnd)
		i++
	}

	if filter.Count > 0 {
		buff.WriteString(fmt.Sprintf(" limit $%v ", i))
		data = append(data, filter.Count)
		i++
	}

	return buff.String(), data, nil
}
