package search

import (
	"database/sql"
	"reflect"
	"strings"
	"testing"
)

func Test_generateSQL(t *testing.T) {
	type args struct {
		filter Filter
	}
	tests := []struct {
		name    string
		args    args
		want    string
		want1   []interface{}
		wantErr bool
	}{
		{
			name: "Test_patient_id",
			args: args{
				filter: Filter{
					PatientID: "id1",
				},
			},
			want:    strings.Replace(sqlStudy, "\n", " ", -1) + "where (st.nu_ltaexported !=0)  and p.patientid= $1 ",
			want1:   []interface{}{"id1"},
			wantErr: false,
		},
		{
			name: "Test_dateInit",
			args: args{
				filter: Filter{
					PatientID: "id1",
					DateInit:  "20190101",
				},
			},
			want:    strings.Replace(sqlStudy, "\n", " ", -1) + "where (st.nu_ltaexported !=0)  and p.patientid= $1  and st.studydate >= $2 ",
			want1:   []interface{}{"id1", "20190101"},
			wantErr: false,
		},
		{
			name: "Test_without_patientid_and_count",
			args: args{
				filter: Filter{},
			},
			want:    "",
			want1:   make([]interface{}, 0),
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, err := generateSQL(&tt.args.filter)
			if (err != nil) != tt.wantErr {
				t.Errorf("generateSQL() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("generateSQL() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("generateSQL() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestSearch_Search(t *testing.T) {
	type fields struct {
		DB *sql.DB
	}
	type args struct {
		filter Filter
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    Result
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := Search{
				DB: tt.fields.DB,
			}
			cgot, err := s.Search(&tt.args.filter)
			if (err != nil) != tt.wantErr {
				t.Errorf("Search.Search() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			got := <-cgot
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Search.Search() = %v, want %v", got, tt.want)
			}
		})
	}
}

func getConection() *sql.DB {
	return nil
}
