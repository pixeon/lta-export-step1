package service

import (
	"os"

	"bitbucket.org/pixeon/lta-export-step1/search"
	"github.com/rs/zerolog/log"
)

// NotFounds is the struct os images not founds
type NotFounds struct {
	search.Result
	Notfounds [][2]string
	Founds    [][2]string
}

// Check if all dicom images are in the correct place
func Check(results chan search.Result) (chan search.Result, chan NotFounds) {

	cOk := make(chan search.Result, 5)
	cNF := make(chan NotFounds, 20)

	go func() {
		// TODO try to do it in parallel
		for r := range results {
			var aOk, aNF [][2]string
			for _, d := range r.Dicoms {
				if isThere(&d[1]) {
					aOk = append(aOk, d)
				} else {
					aNF = append(aNF, d)
				}
			}

			log.Info().Msgf("Check: study %s found %d , not found %d", r.StudyUID, len(aOk), len(aNF))
			if len(aNF) < 1 {
				cOk <- r
			} else {
				nf := NotFounds{
					Notfounds: aNF,
					Result:    r,
					Founds:    aOk,
				}
				cNF <- nf
			}
		}
		close(cNF)
	}()

	return cOk, cNF
}

func isThere(path *string) bool {
	if _, err := os.Stat(*path); err == nil {
		return true
	}
	return false //os.IsNotExist(err)

}
