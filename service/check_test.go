package service_test

import (
	"testing"

	"bitbucket.org/pixeon/lta-export-step1/search"
	"bitbucket.org/pixeon/lta-export-step1/service"
)

func TestCheck(t *testing.T) {

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ch := make(chan search.Result, len(tt.args.results))
			for _, r := range tt.args.results {
				ch <- r
			}
			cOk, cErr := service.Check(ch)
			var gotOk, gotErr bool

			select {
			case <-cOk:
				gotOk = true
			case <-cErr:
				gotErr = true
			}

			if tt.wantOk != gotOk {
				t.Errorf("Check() Ok chan got = %v, want %v", gotOk, tt.wantOk)
			}

			if tt.wantErr != gotErr {
				t.Errorf("Check() Error chan got = %v, want %v", gotErr, tt.wantErr)
			}

		})
	}
}
