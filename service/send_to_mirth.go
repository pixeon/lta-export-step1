package service

import (
	"bitbucket.org/pixeon/lta-export-step1/search"
	"github.com/rs/zerolog/log"
)

// Sender is the interface that represents the dependency of inserting the ok images
type Sender interface {
	Insert(string, string, string, string) error
}

// Send images to second step (Mirth)
func Send(ch chan search.Result, s Sender) chan int64 {
	done := make(chan int64)

	go func() {
		var totalDicom int64
		for r := range ch {

			for _, dicom := range r.Dicoms {
				err := s.Insert(r.PatientID, r.StudyUID, dicom[0], dicom[1])
				if err != nil {
					log.Error().Err(err).Msgf("Error on insert PatientID: %s StudyUID %s path %s \n  %v \n", r.PatientID, r.StudyUID, dicom, err)
				} else {
					totalDicom++
				}
			}
			log.Info().Msgf("Inserted %v of PatientID: %s StudyUID %s", len(r.Dicoms), r.PatientID, r.StudyUID)

		}
		done <- totalDicom
	}()

	return done
}
