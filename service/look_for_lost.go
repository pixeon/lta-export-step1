package service

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"

	"bitbucket.org/pixeon/lta-export-step1/search"
	"github.com/rs/zerolog/log"
)

// Find Try to find dicom image in others storages
func Find(ch chan NotFounds, cOk chan search.Result, folderCsv string) chan int64 {
	done := make(chan int64)
	go func() {
		var c int
		for nf := range ch {

			log.Info().Msgf("Searching %d imgs of study %s", len(nf.Notfounds), nf.StudyUID)

			founds := [][2]string{}
			notFounds := []string{}
			for _, path := range findOtherPathCsvFile(nf.PatientID, folderCsv) {

				for _, img := range nf.Notfounds {
					newDicomPath := fmt.Sprintf("%s%v%s.dcm", path, string(os.PathSeparator), img[0])

					if isThere(&newDicomPath) {
						log.Info().Msgf("Found in new path %s", newDicomPath)
						founds = append(founds, [2]string{img[0], newDicomPath})
					} else {
						notFounds = append(notFounds, img[0])
						c++
					}
				}
			}

			if len(nf.Notfounds) == len(founds) {
				nf.Result.Dicoms = append(nf.Founds, founds...)
				cOk <- nf.Result
			} else {
				if len(notFounds) < 1 {
					for _, i := range nf.Notfounds {
						notFounds = append(notFounds, i[0])
					}
				}
				writeToNotFound(nf.Result, notFounds)
			}

		}
		done <- int64(c)
	}()
	return done
}

func findOtherPathCsvFile(patientID string, folderCsv string) []string {
	paths := []string{}
	log.Info().Msgf("search patientID %s in csv", patientID)

	f, e := os.Open(folderCsv)
	if e != nil {
		log.Error().Err(e).Msgf("could not open %s", folderCsv)
		return paths
	}
	defer f.Close()
	// Create a new reader.
	r := csv.NewReader(bufio.NewReader(f))
	r.Comma = ';'
	for {
		record, err := r.Read()
		// Stop at EOF.
		if err == io.EOF {
			break
		}

		if c, e := strconv.Atoi(record[3]); e != nil || c < 1 {
			continue
		}
		if strings.Contains(record[1], patientID) {
			log.Info().Msgf("possible path: %s", record[1])
			paths = append(paths, record[1])
		}
	}
	return paths
}

func writeToNotFound(r search.Result, imgs []string) {
	log.Info().Msgf("writeToNotFound  PatientID %s, img count %d", r.PatientID, len(imgs))
	file, err := os.OpenFile("notFound.csv", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Error().Err(err).Msg("could not create notFound.csv")
		return
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	writer.Comma = ';'
	defer writer.Flush()

	for _, img := range imgs {
		err := writer.Write([]string{r.PatientID, r.StudyUID, img})
		if err != nil {
			log.Error().Err(err).Msgf("error to write PatientID %s, img %s", r.PatientID, img)
		}
	}
}
