package service_test

import (
	"bitbucket.org/pixeon/lta-export-step1/search"
	"bitbucket.org/pixeon/lta-export-step1/test"
)

type args struct {
	results []search.Result
}

var tests = []struct {
	name    string
	args    args
	wantOk  bool
	wantErr bool
}{
	{
		name:   "Exist_files",
		wantOk: true,
		args: args{
			results: []search.Result{
				search.Result{
					Dicoms: [][2]string{
						[2]string{"", test.GetProjectDir() + "/test/test.txt"},
						[2]string{"", test.GetProjectDir() + "/test/test2.txt"},
					},
				},
			},
		},
	},
	{
		name:    "Not_exist_file",
		wantErr: true,
		args: args{
			results: []search.Result{
				search.Result{
					Dicoms: [][2]string{
						[2]string{"", test.GetProjectDir() + "/test/notfound.txt"},
						[2]string{"", test.GetProjectDir() + "/test/test.txt"},
						[2]string{"", test.GetProjectDir() + "/test/test2.txt"},
					},
				},
			},
		},
	},
}
