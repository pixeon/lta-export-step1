package main

import (
	"bufio"
	"database/sql"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"

	"bitbucket.org/pixeon/lta-export-step1/repository"

	"bitbucket.org/pixeon/lta-export-step1/search"
	"bitbucket.org/pixeon/lta-export-step1/service"
	_ "github.com/lib/pq"
	"github.com/rs/zerolog/log"
)

func main() {
	prop := flag.String("properties", "config.properties", "Path of config")
	folders := flag.String("folders", "data/folders.csv", "Path of folders csv")

	flag.Parse()

	props, err := readPropertiesFile(*prop)
	checkError(err)

	conLTA, err := connect(props, "lta")
	checkError(err)

	conPacs, err := connect(props, "pacs")
	checkError(err)

	s := search.NewSearch(conPacs)
	rep := repository.NewRepository(conLTA)

	filter := getFilter(props)

	results, err := s.Search(filter)
	checkError(err)

	cOk, cNotFound := service.Check(results)

	doneOk := service.Send(cOk, rep)
	doneNF := service.Find(cNotFound, cOk, *folders)

	//service.Find writes in the channel cok, so it should close first
	countNotFound := <-doneNF
	close(cOk)
	countSend := <-doneOk

	log.Info().Msgf("Finishing job: %d sent to step 2 and %d has not found yet", countSend, countNotFound)

}

func getFilter(prop map[string]string) *search.Filter {
	patient := getProp(prop, "filter.patient.id", false)
	datei := getProp(prop, "filter.date.init", false)
	datee := getProp(prop, "filter.date.end", false)
	studyuid := getProp(prop, "filter.studyuid", false)
	count, e := strconv.Atoi(getProp(prop, "filter.count", false))
	checkError(e)

	return &search.Filter{
		PatientID: patient,
		Count:     count,
		DateEnd:   datee,
		DateInit:  datei,
		StudyUID:  studyuid,
	}
}

func getProp(prop map[string]string, key string, required bool) (value string) {
	value, e := prop[key]
	if !e && required {
		log.Fatal().Msgf("Error: %s is required!", key)
	}
	return
}

func readPropertiesFile(filename string) (map[string]string, error) {
	config := map[string]string{}

	if len(filename) == 0 {
		return config, nil
	}
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal().Err(err).Msgf("open file %s", filename)
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if equal := strings.Index(line, "="); equal >= 0 {
			if key := strings.TrimSpace(line[:equal]); len(key) > 0 {
				value := ""
				if len(line) > equal {
					value = strings.TrimSpace(line[equal+1:])
				}
				config[key] = value
			}
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal().Err(err)
		return nil, err
	}

	return config, nil
}

func checkError(err error) {
	if err != nil {
		log.Fatal().Err(err)
	}
}

func connect(props map[string]string, prefix string) (*sql.DB, error) {
	host, port, user, password, dbname := props[prefix+".db.host"], props[prefix+".db.port"],
		props[prefix+".db.user"], props[prefix+".db.password"], props[prefix+".db.database"]

	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		return nil, err
	}

	return db, nil
}
