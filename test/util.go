package test

import (
	"os"
	"path/filepath"
	"strings"
)

func GetProjectDir() string {
	p, _ := os.Getwd()
	for !strings.HasSuffix(p, "lta-export-step1") {
		p = filepath.Dir(p)
	}
	return p
}
