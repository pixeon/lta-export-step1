# check-pacs-storage

**Primeira parte** do projeto de enviar as imagens antigas para o LTA

## objetivo
1 - Buscar os exames antigos 

2 - Validar se todas as imagens estão so seu storage corretamente

3 - Caso afirmativo, enviar para a **Segunda parte** do projeto

4 - Caso negativo, procurar as imagens perdidas em _outros storages_

5 - Caso encontre, executar o passo 3

## Build
```
GOOS=windows GOARCH=amd64 CGO_ENABLED=0 go build -v -a -installsuffix cgo -o busca.exe -ldflags "-X main.version=1.1 -X main.build=1.1 " ./main/
```


## Run
-properties -> caminho do arquivo de properties com as informações de conexão com o banco

-folders -> caminho para o csv com a lista de storages
```
./busca -properties=config.properties -folder=data/folders.csv
```

